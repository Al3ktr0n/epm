/* eslint-disable */
const pool = require('./pool');
const students = require('./students.js');

const get = (id) => {
  return new Promise((resolve, reject) => {
    pool.query(
      `SELECT * FROM tables where id = ${id} AND deleted != 1`,
      async (err, rows) => {
        console.log(rows);
        if (rows.length > 0) {
          rows[0].students = await students
            .getTableStudents(rows[0].id)
            .catch((err) => {
              for (index of rows[0].students) index.values = null;
            });
          resolve(rows[0]);
        } else {
          reject('no table');
        } 
      }
    );
  });
};

const getByHash = (hash) => {
  // console.log(hash);
  return new Promise((resolve, reject) => {
    pool.query(
      `SELECT * FROM tables where hashID = '${hash}' AND deleted != 1`,
      async (err, rows) => {
        console.log(rows);
        if (rows.length > 0) {
          resolve(rows[0]);
        } else {
          reject('no table');
        } 
      }
    );
  });
};

const removedeleted = () => {
  return new Promise((resolve, reject) => {
    pool.query(`DELETE FROM tables where deleted = '1'`, async (err, succ) => {
      if (err) {
        console.log(err);
      }
    });
  });
};

const getAll = () => {
  return new Promise((resolve) => {
    pool.query(
      `SELECT * FROM tables where deleted != '1'`,
      async (err, rows) => {
        for (row of rows) {
          row.students = await students.getTableStudents(row.id);
        }
        resolve(rows);
      }
    );
  });
};
const getUser = (userId) => {
  return new Promise((resolve, reject) => {
    pool.query(
      `SELECT * FROM tables where created_id = ${userId} AND deleted != 1`,
      async (err, rows) => {
        if (rows) {
          for (row of rows) {
            row.students = await students.getTableStudents(row.id);
            console.log(row.students);
          }
          console.log(rows);
          resolve(rows);
        } else {
          resolve(null);
        }
      }
    );
  });
};
const update = (id, data) => {
  return new Promise((resolve, reject) => {
    get(id)
      .then((table) => {
        pool.query(
          `UPDATE tables SET ? WHERE id = ${table.id}`,
          data,
          (err, rows) => {
            resolve(rows);
          }
        );
      })
      .catch((err) => reject(err));
  });
};

/* const remove = (id) => {
  return new Promise((resolve, reject) => {
    const data = {
      deleted_at: new Date(),
      deleted: 1
    };
    get(id)
      .then((table) => {
        pool.query(
          `UPDATE tables SET ? WHERE id = ${table.id}`,
          data,
          (err, rows) => {
            resolve(true);
          }
        );
      })
      .catch((err) => reject(err));
  });
}; */
const remove = (id) => {
  return new Promise((resolve, reject) => {
    get(id)
      .then((table) => {
        const data = {
          deleted_at: new Date(),
          deleted: true
        };
        pool.query(
          `UPDATE tables SET ? WHERE id = ${table.id}`,
          data,
          (err, result) => {
            resolve(true);
          }
        );
      })
      .catch((err) => reject(err));
  });
};

const create = (data, id) => {
  return new Promise((resolve) => {
    // data.created_at = new Date(),
    (data.deleted = false), data.name == null ? 'new table' : data.name;
    data.created_id = id;
    console.log(id);
    pool.query(`INSERT INTO tables SET ?`, data, (err, result) => {
      if (err) {
        console.log(err);
      }
      console.log(data, result);
      resolve('Success');
    });
  });
};

module.exports.create = create;
module.exports.update = update;
module.exports.remove = remove;
module.exports.getAll = getAll;
module.exports.get = get;
module.exports.removedeleted = removedeleted;
module.exports.getUser = getUser;
module.exports.get_by_hash = getByHash;

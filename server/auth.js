module.exports = ({ pool, express, bcrypt, jwt, jwtToken }) => {
  const routes = express.Router();
  const saltRounds = 15;

  routes.post('/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    if (!email || !password)
      return res.status(400).json({
        type: 'error',
        message: 'email and password fields are essential for authentication.'
      });
    pool.query('SELECT * FROM users WHERE email=?', email, (error, results) => {
      if (error)
        return res
          .status(500)
          .json({ type: 'error', message: 'db error', error });
      if (results.length === 0)
        return res.status(403).json({
          type: 'error',
          message: 'User with provided email not found in database.'
        });
      const user = results[0];
      bcrypt.compare(password, user.password, (error, result) => {
        if (error)
          return res
            .status(500)
            .json({ type: 'error', message: 'bcrypt error', error });
        if (result) {
          res.json({
            type: 'success',
            message: 'User logged in.',
            user: { id: user.id, email: user.email },
            token: jwt.sign({ id: user.id, email: user.email }, jwtToken, {
              expiresIn: '7d'
            })
          });
        } else
          return res.status(403).json({
            type: 'error',
            message: 'Password is incorrect.'
          });
      });
    });
  });

  // Start of register

  routes.post('/register', async (req, res) => {
    console.log('I AM HERE');
    console.log(req.body.surname);
    const userEmail = req.body.email;
    const password = req.body.password;
    const confirmPass = req.body.confirmPass;
    const Name = req.body.name;
    const Surname = req.body.surname;

    if (!userEmail || !password || !confirmPass)
      return res.status(400).json({
        type: 'error',
        message: 'All fields are essential for registration'
      });
    else if (password !== confirmPass)
      return res.status(400).json({
        type: 'error',
        message: 'Passwords mismatch, please, try again'
      });
    const encryptedPassword = await bcrypt.hash(password, saltRounds);
    const users = {
      email: userEmail,
      password: encryptedPassword,
      name: Name,
      surname: Surname
    };
    await pool.query('INSERT INTO users SET ?', users, (error) => {
      if (error) {
        console.log(error);
        return res
          .status(500)
          .json({ type: 'error', message: 'db error', error });
      }
    });
    console.log(typeof userEmail);
    pool.query(
      `SELECT * FROM users WHERE email = \'${userEmail.toString()}\'`,
      (error, result) => {
        if (error) {
          console.log('select', error);
          return res
            .status(500)
            .json({ type: 'error', message: 'db error', error });
        }
        console.log(result);
        if (result.length > 0) {
          console.log(result);
          const user = result[0];
          res.json({
            type: 'success',
            message: 'User created.',
            user: { id: user.id, email: user.email },
            token: jwt.sign({ id: user.id, email: user.email }, jwtToken, {
              expiresIn: '7d'
            })
          });
        }
      }
    );
  });

  // End of register

  routes.get('/me', (req, res) => {
    const token = req.headers['x-access-token'];
    if (!token)
      return res.status(400).json({
        type: 'error',
        message: 'x-access-token header not found.'
      });
    jwt.verify(token, jwtToken, (error, result) => {
      if (error)
        return res.status(403).json({
          type: 'error',
          message: 'Provided token is invalid.',
          error
        });
      return res.json({
        type: 'success',
        message: 'Provided token is valid.',
        result
      });
    });
  });

  return routes;
};

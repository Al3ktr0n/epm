/* eslint-disable */
const express = require('express');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const mysql = require('mysql');
const consola = require('consola');
const { Nuxt, Builder } = require('nuxt');
const config = require('../nuxt.config.js');
const jwtConf = require('../jwtToken');
const sha256 = require('sha256');

const app = express();
const fs = require('fs');
const students = require('./students');
const tables = require('./tables');
const tasks = require('./tasks');
const pool = require('./pool.js');
const auth = require('./autm');
const lobby = require('./lobby');
const users = require('./users');
const { table } = require('console');

// columns.create(5, { name: 'test' }).then(rows.createbystudent(5, 22));
// rows.createbystudent({});
config.dev = process.env.NODE_ENV !== 'production';
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
// app.use('/auth', auth({ pool, express, bcrypt, jwt, jwtToken: jwtConf }));
// GET methods

app.get('/red', function(req, res) {
  tables.getAll().then((rows) => {
    res.send(JSON.stringify(rows));
  });
});
app.get('/get_table_tasks', function(req, res) {
  tasks
    .getbytbl(parseInt(req.query.id))
    .then((rows) => {
      if (rows) {
        res.send(JSON.stringify(rows));
      } else {
        res.send(null);
      }
    })
    .catch((err) => {
      res.send('no');
    });
});
app.get('/get_table_by_id', async (req, res) => {
  tables.get(req.query.id).then((rows) => {
    res.send(JSON.stringify(rows));
  });
});
// POST method route
app.post('/edit_student', async (req, res) => {
  await students.update(req.body.table_id, req.body.id, req.body.data);
  tables.getAll().then(async (rows) => {
    res.send(rows);
  });
});
app.post('/delete_task', async (req, res) => {
  await tasks.remove(req.body.id);
  tasks
    .getbytbl(req.body.tableId)
    .then(async (rows) => {
      res.send(rows);
    })
    .catch((err) => {
      res.send('nope');
    });
});
app.post('/login', async (req, res) => {
  await auth
    .login(req.body.email, req.body.password)
    .then((suc) => {
      if (suc.type == 'success') {
        return res.status(200).json(suc);
      }
    })
    .catch((err) => {
      console.log(err);
      return res.status(403).json(err.type);
    });
});
app.get('/me', (req, res) => {
  const token = req.headers['x-access-token'];
  if (!token)
    return res.status(400).json({
      type: 'error',
      message: 'x-access-token header not found.'
    });
  jwt.verify(token, jwtConf, (error, result) => {
    if (error)
      return res.status(403).json({
        type: 'error',
        message: 'Provided token is invalid.',
        error
      });
    return res.json({
      type: 'success',
      message: 'Provided token is valid.',
      result
    });
  });
});
app.post('/register', async (req, res) => {
  console.log(req.body.name);
  await auth.register(
    req.body.email,
    req.body.password,
    req.body.name,
    req.body.surname
  );
  auth.login(req.body.email, req.body.password).then((suc) => {
    return res.status(200).json(suc);
  });
});
app.post('/edit_task', async (req, res) => {
  await tasks.edit(req.body.data.id, {
    comment: req.body.data.comment,
    task: req.body.data.task
  });
  tasks
    .getbytbl(req.body.data.tableId)
    .then(async (rows) => {
      res.send(rows);
    })
    .catch((err) => {
      res.send('nope');
    });
});

app.post('/create_invite', async (req, res) => {
  tableID = req.body.tableID.toString();
  tableName = req.body.tableName;
  hashID = sha256(tableID);
  // console.log(hashID);
  await tables.update(tableID, { hashID: hashID })
});

app.post('/add_student_into_table', async (req, res) => {
  console.log('Adding student');
  userData = req.body.userData;
  hash = req.body.hash;
  userID = userData.id;
  await users.get(userID).then(async (users_res) => {
    // console.log('printing users_res', users_res);
    await tables.get_by_hash(hash).then(async (tables_res) => {
      // console.log(tables_res);
      // console.log('printing users_res again', users_res);
      // console.log(users_res.name);
      data = { 
        created_at: new Date(), 
        deleted: false, 
        table_id: tables_res.id, 
        user_id: users_res.id, 
        name: users_res.name + ' ' + users_res.surname 
      }
      console.log(data);
      await students.create(tables_res.id, data)
      .catch((err) => {
        console.log(err);
      })
      .then((res) => {
          console.log(res);
      });
    });
  })
});

app.get('/team_name_by_hash', async (req, res) => {
  hash = req.query.hash;
  hash = hash.toString();
  // console.log(hash);
  await tables
    .get_by_hash(hash)
    .catch((err) => {
      console.log(err);
    })
    .then((row) => {
      console.log(row);
      res.send(row);
    });
  // res.send(hash);
});

app.post('/create_task', async (req, res) => {
  await tasks
    .add(
      req.body.data,
      JSON.stringify({ groups: null, tables: [parseInt(req.body.tableId)] })
    )
    .catch((err) => {
      console.log(err);
    });
  tasks
    .getbytbl(req.body.tableId)
    .then(async (rows) => {
      console.log('here');
      res.send(rows);
    })
    .catch((err) => {
      console.log('heree');
      res.send('nope');
    });
});
app.post('/complete_task', async (req, res) => {
  await tasks.complete(req.body.id, req.body.data);
  tables
    .getAll()
    .then(async (rows) => {
      res.send(rows);
    })
    .catch((err) => {
      res.send('nope');
    });
});
app.post('/create_student', async (req, res) => {
  console.log(req.body.data);
  await students.create(req.body.table_id, req.body.data);
  tables.getAll().then(async (rows) => {
    res.send(rows);
  });
});
app.post('/delete_table', async (req, res) => {
  await tables.remove(req.body.table_id);
  tables.getAll().then(async (rows) => {
    res.send(rows);
  });
});
app.post('/add_task', async (req, res) => {
  await tasks.add(req.body.data, req.body.json);
  tasks.getbytbl(req.body.table_id).then(async (rows) => {
    res.send(rows);
  });
});
app.post('/delete_student', async (req, res) => {
  await students.remove(req.body.table_id, req.body.id);
  tables.getAll().then(async (rows) => {
    res.send(rows);
  });
});
app.post('/create_table', async (req, res) => {
  console.log('Got on backend');
  console.log(req.body.id);
  await tables.create({ name: req.body.name }, req.body.id);
  tables.getUser(req.body.id).then(async (rows) => {
    res.send(rows);
  });
});
app.get('/user_tables', async (req, res) => {
  console.log(req.query.id);
  tables
    .getUser(req.query.id)
    .then((rows) => {
      res.send(rows);
    })
    .catch((err) => {
      console.log(err);
    });
});
app.get('/get_student', (req, res) => {
  students.get(req.query.id).then((row) => {
    res.send(row[0]);
  });
});

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config);

  const { host, port } = nuxt.options.server;

  await nuxt.ready();
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt);
    await builder.build();
  }

  // Give nuxt middleware to express
  app.use(nuxt.render);
  // Listen the server
  app.listen(port, host);
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  });
}
start();

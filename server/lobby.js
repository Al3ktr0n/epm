const pool = require('./pool');

// добавление студентов через их почту
const addStudentIntoLobby = (userEmail, hash) => {
  console.log(userEmail);
  return new Promise((resolve, reject) => {
    pool.query(
      // вопрос: можно ли этот дублируемый кусок кода вынести в функцию?
      `SELECT * FROM lobby where Hash = '${hash}' AND Deleted != 1`,
      // eslint-disable-next-line require-await
      async (err, rows) => {
        if (err) {
          reject(err);
        }
        if (rows.length < 1) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject('lobby not found');
        } else {
          // JSON в БД сохраняется как строка, его обязательно нужно парсить
          const studentsJSON = JSON.parse(rows[0].Students);
          let failed = false;
          console.log(studentsJSON);
          for (let i = 0; i < studentsJSON.length; i++) {
            if (studentsJSON[i] == userEmail) {
              // eslint-disable-next-line prefer-promise-reject-errors
              reject('user has already been added');
              // reject почему-то не работает как return, поэтому добавляем булеву проверку 
              failed = true;
            }
          }
          if (!failed) {
            pool.query(
              // нельзя просто так взять и обновить JSON-файл в БД
              `UPDATE lobby SET Students = JSON_ARRAY_APPEND(Students, '$', "${userEmail}") WHERE Hash = '${hash}' AND Deleted != 1`,
              // eslint-disable-next-line require-await
              async (err, res) => {
                if (err) {
                  reject(err);
                } else {
                  resolve(res);
                }
              }
            );
          }
        }
      }
    );
  });
};

// по хешу ищем название лобби, требуется для красивого отображения приглашалок
const getName = (hash) => {
  return new Promise((resolve, reject) => {
    pool.query(
      `SELECT * FROM lobby where Hash = '${hash}' AND Deleted != 1`,
      // eslint-disable-next-line require-await
      async (err, rows) => {
        if (err) {
          reject(err);
        }
        if (rows.length < 1) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject('no such lobby');
        } else {
          resolve(rows[0]);
        }
      }
    );
  });
};

const createLobby = (hash, name) => {
  // hash = toString(hash);
  return new Promise((resolve, reject) => {
    pool.query(
      `SELECT * FROM lobby where Hash = '${hash}' AND Deleted != 1`,
      // eslint-disable-next-line require-await
      async (err, rows) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        if (rows.length > 0) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject('lobby has already been created');
        } else {
          pool.query(
            `INSERT INTO lobby SET Hash = '${hash}', LobbyName = '${name}', Students = JSON_ARRAY()`,
            (err, result) => {
              if (err) {
                console.log(err);
                reject(err);
              }
              console.log(hash, result);
              resolve('Success');
            }
          );
        }
      }
    );
  });
};

module.exports.create_lobby = createLobby;
module.exports.get_name = getName;
module.exports.add_student_into_lobby = addStudentIntoLobby;
